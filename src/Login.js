import React, {Component} from 'react'
import * as firebase from 'firebase/app';

class Login extends Component {

    constructor() {
        super();
        this.state = {
            user:null
        };

    this.handleAuth = this.handleAuth.bind(this);
    this.handleLogout = this.handleLogout.bind(this);
    }

    componentDidMount () {
        firebase.auth().onAuthStateChanged(user => {
            this.setState({user});
        });
    }

    handleAuth() {
        const provider = new firebase.auth.GoogleAuthProvider();

        firebase.auth().signInWithPopup(provider)
        .then(result => console.log(`${result.user.email} ha iniciado sesion`))
        .catch(error => console.log(`error ${error.code}: ${error.message}`));
    }

    handleLogout() {
        firebase.auth().signOut()
        .then(result => console.log(`${result.user.email} ha salido`))
        .catch(error => console.log(`error ${error.code}: ${error.message}`));
    }

    renderLoginButton() {
        //si el usuario esta logeado
        if(this.state.user){
            return(
                <div>
                    <img src={this.state.user.photoURL} alt={this.state.user.displayName}></img>
                    <p>hola {this.state.user.displayName}!</p>
                    <button onClick={this.handleLogout}>salir</button>
                </div>
            );
        }else {
            return(<button onClick={this.handleAuth}>Login con Google </button>);
            
        }
        //y si no lo esta
        
    }

    render() {
        return (
            <div>
                {this.renderLoginButton()}
            </div>
        );
    }
}

export default Login;
